# File testheap.py
# This file provides some testing for the class Heap
# You will want to test siftUpFrom and siftDownFromTo separately!

from heap import Heap, heapSort

from person import Person

def main():
   # 1
   heap = Heap()
   heap.addToHeap(20)
   heap.addToHeap(40)
   heap.addToHeap(30)
   heap.addToHeap(50)
   heap.addToHeap(45)
   heap.addToHeap(100)
   heap.addToHeap(10)
   print( heap )
   
   # 2
   heap = Heap(4,False)
   heap.addToHeap(20)
   heap.addToHeap(40)
   heap.addToHeap(30)
   heap.addToHeap(50)
   heap.addToHeap(45)
   heap.addToHeap(100)
   heap.addToHeap(10)
   print( heap )

   # 3
   aList = [15, -34, 23, 46, -4, 20]
   heap = Heap()
   heap.buildFrom(aList)
   print( heap )
 
   #4
   aList = [15, -34, 23, 46, -4, 20]
   print( heapSort(aList) )
   
   #5
   aList = [15, -34, 23, 46, -4, 20]
   print( heapSort(aList, False) )

   
   #6
   heap = Heap(numberOfChildren = 4)
   heap.buildFrom((10, 20, -29, 16, 70, 30, 20, 100, 38, -293, \
     77, -19, -77, 230, 91, -230, -48, 23))
   print( heap )

   a = heapSort((10, 20, -29, 16, 70, 30, 20, 100, 38, -293, \
     77, -19, -77, 230, 91, -230, -48, 23))
   print( a )
   
   a = Person('Fred', 10)
   b = Person('Joe', -29)
   c = Person('Jill', 70)
   d = Person('Walt', 100)
   e = Person('Doug', -293)
   f = Person('Jim', 30)
   g = Person('Julie', -77)
   print( heapSort([a,b,c,d,e,f,g]) )
   print( heapSort([a,b,c,d,e,f,g], False) )

  
if __name__ == '__main__': main()

'''  The following is the expected output from testheap:
It is a largest on top heap:
The size of the heap is 7.
The capacity of the heap is 10.
The elements of the heap are: 
100
50
30
40
20
45
10

It is a smallest on top heap:
The size of the heap is 7.
The capacity of the heap is 8.
The elements of the heap are: 
10
20
30
50
45
100
40

It is a largest on top heap:
The size of the heap is 6.
The capacity of the heap is 6.
The elements of the heap are: 
46
20
23
15
-4
-34

[-34, -4, 15, 20, 23, 46]
[46, 23, 20, 15, -4, -34]
It is a largest on top heap:
The size of the heap is 18.
The capacity of the heap is 18.
The elements of the heap are: 
230
100
77
91
70
30
20
20
38
-293
-29
-19
-77
16
10
-230
-48
23

[-293, -230, -77, -48, -29, -19, 10, 16, 20, 20, 23, 30, 38, 70, 77, 91, 100, 230]
[Name: Doug Id: -293 , Name: Julie Id: -77 , Name: Joe Id: -29 , Name: Fred Id: 10 , Name: Jim Id: 30 , Name: Jill Id: 70 , Name: Walt Id: 100 ]
[Name: Walt Id: 100 , Name: Jill Id: 70 , Name: Jim Id: 30 , Name: Fred Id: 10 , Name: Joe Id: -29 , Name: Julie Id: -77 , Name: Doug Id: -293 ]

'''
